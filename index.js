const express = require('express');
const path = require('path');

const app = express();
const port = 9500;

app.engine('.html', require('ejs').__express);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'html');

app.get('/', (req, res) => {
    res.render('main', {
        token: req.query.token
    });
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});